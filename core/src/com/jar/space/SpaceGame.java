package com.jar.space;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.jar.space.screens.LoadingScreen;

public class SpaceGame extends Game {
    private SpriteBatch batch;
	private AssetManager assetManager;

	@Override
	public void create () {
		Gdx.app.setLogLevel(Application.LOG_DEBUG);

		batch = new SpriteBatch();
		assetManager = new AssetManager();

        setScreen(new LoadingScreen(this));
	}

    @Override
	public void dispose () {
		batch.dispose();
        assetManager.dispose();
	}

    public AssetManager getAssetManager() {
        return assetManager;
    }

    public SpriteBatch getBatch() {
        return batch;
    }
}
