package com.jar.space.util;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * Created by Marcus on 01.02.2017.
 */

public final class AssetDescriptors {
    public static final AssetDescriptor<TextureAtlas> GAMEPLAY_ATLAS =
            new AssetDescriptor<TextureAtlas>("gameplay/gameplay.atlas", TextureAtlas.class);

    public static final AssetDescriptor<Skin> SKIN =
            new AssetDescriptor<Skin>("ui/uiskin.json", Skin.class);

    private AssetDescriptors() {}
}
