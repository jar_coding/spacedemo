package com.jar.space.screens;


import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.jar.space.config.GameConfig;

/**
 * Created by Jar on 13.03.2017.
 */

public class HealthBarWidget extends Table {

    private static final String HEALTH_TEXTURE_PATH = "health";
    private static final int MAX_HEALTH = 10;

    private Image[] health;

    public HealthBarWidget(TextureAtlas atlas) {
        this.health = new Image[MAX_HEALTH];

        TextureRegion region = atlas.findRegion(HEALTH_TEXTURE_PATH);
        padLeft(20);
        defaults().padLeft(5);
        setPosition(0, GameConfig.HUD_HEIGHT - region.getRegionHeight() - 20);

        for(int i = 0; i < MAX_HEALTH; i++) {
            this.health[i] = new Image(region);
        }

        update(10);
    }

    public void update(int health) {
        if(health > MAX_HEALTH) {
            return;
        }

        clear();
        for(int i = 0; i < health; i++) {
            add(this.health[i]);
        }
        pack();
    }
}
