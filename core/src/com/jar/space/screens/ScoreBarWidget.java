package com.jar.space.screens;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.jar.space.config.GameConfig;

/**
 * Created by Marcus on 17.03.2017.
 */

public class ScoreBarWidget extends Table {
    private Label score;

    public ScoreBarWidget(Skin skin) {
        this.score = new Label("0", skin);
        this.score.setAlignment(Align.right);

        setPosition(GameConfig.HUD_WIDTH - 232, GameConfig.HUD_HEIGHT - 32);
        add(score).maxWidth(200).minWidth(200).left().expand();
    }

    public void setScore(int score) {
        this.score.setText(String.valueOf(score));
    }
}
