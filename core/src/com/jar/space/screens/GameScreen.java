package com.jar.space.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.jar.space.SpaceGame;
import com.jar.space.ashley.PoolEngine;
import com.jar.space.ashley.systems.BoundsSystem;
import com.jar.space.ashley.systems.CleanUpSystem;
import com.jar.space.ashley.systems.CollisionSystem;
import com.jar.space.ashley.systems.CreateMeteoritesSystem;
import com.jar.space.ashley.systems.EntityFactory;
import com.jar.space.ashley.systems.GameOverSystem;
import com.jar.space.ashley.systems.LoseHealthSystem;
import com.jar.space.ashley.systems.MovementSystem;
import com.jar.space.ashley.systems.PlayerInputSystem;
import com.jar.space.ashley.systems.RemoveMeteoritesSystem;
import com.jar.space.ashley.systems.RestartSystem;
import com.jar.space.ashley.systems.UpdateScoreSystem;
import com.jar.space.ashley.systems.debug.DebugRenderSystem;
import com.jar.space.ashley.systems.initial.InitBackgroundSystem;
import com.jar.space.ashley.systems.initial.InitPlayerSystem;
import com.jar.space.ashley.systems.render.RenderHealthSystem;
import com.jar.space.ashley.systems.render.RenderScoreSystem;
import com.jar.space.ashley.systems.render.RenderSystem;
import com.jar.space.config.GameConfig;
import com.jar.space.util.AssetDescriptors;
import com.jar.space.util.GdxUtils;

/**
 * Created by Jar on 13.03.2017.
 */

public class GameScreen implements Screen {
    private final SpaceGame game;
    private final TextureAtlas atlas;
    private final Skin skin;

    private Viewport hudViewport;
    private Viewport gameViewport;

    private PoolEngine pool;
    private EntityFactory factory;

    private Stage stage;

    public GameScreen(SpaceGame game) {
        this.game = game;
        this.atlas = game.getAssetManager().get(AssetDescriptors.GAMEPLAY_ATLAS);
        this.skin = game.getAssetManager().get(AssetDescriptors.SKIN);
    }

    @Override
    public void show() {
        gameViewport = new FitViewport(GameConfig.WORLD_WIDTH, GameConfig.WORLD_HEIGHT);
        hudViewport = new FitViewport(GameConfig.HUD_WIDTH, GameConfig.HUD_HEIGHT);

        pool = new PoolEngine();
        factory = new EntityFactory(pool, atlas);
        stage = new Stage(hudViewport);

        addSystems();
    }

    private void addSystems() {
        pool.addSystem(new InitBackgroundSystem(factory));
        pool.addSystem(new InitPlayerSystem(factory));

        pool.addSystem(new BoundsSystem(gameViewport));
        pool.addSystem(new CreateMeteoritesSystem(factory));
        pool.addSystem(new RemoveMeteoritesSystem(pool));
        pool.addSystem(new CleanUpSystem());
        pool.addSystem(new CollisionSystem(pool));
        pool.addSystem(new LoseHealthSystem());

        pool.addSystem(new MovementSystem());
        pool.addSystem(new PlayerInputSystem(gameViewport));

        pool.addSystem(new RenderSystem(gameViewport, game.getBatch()));
        pool.addSystem(new RenderHealthSystem(stage, atlas));
        pool.addSystem(new RenderScoreSystem(stage, skin));
        pool.addSystem(new DebugRenderSystem(gameViewport));

        pool.addSystem(new GameOverSystem(pool));
        pool.addSystem(new RestartSystem(pool, factory));
        pool.addSystem(new UpdateScoreSystem());
    }

    @Override
    public void render(float delta) {
        GdxUtils.clearScreen();
        stage.act(delta);
        pool.update(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        gameViewport.update(width, height, true);
        hudViewport.update(width, height, true);
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
