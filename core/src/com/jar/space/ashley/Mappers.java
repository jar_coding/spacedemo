package com.jar.space.ashley;

import com.badlogic.ashley.core.ComponentMapper;
import com.jar.space.ashley.components.IncrementScoreComponent;
import com.jar.space.ashley.components.HealthComponent;
import com.jar.space.ashley.components.ScoreComponent;
import com.jar.space.ashley.components.PositionComponent;
import com.jar.space.ashley.components.SizeComponent;
import com.jar.space.ashley.components.TextureComponent;
import com.jar.space.ashley.components.VelocityComponent;

/**
 * Created by Marcus on 14.03.2017.
 */

public class Mappers {
    public static final ComponentMapper<PositionComponent> position =
            ComponentMapper.getFor(PositionComponent.class);

    public static final ComponentMapper<HealthComponent> health =
            ComponentMapper.getFor(HealthComponent.class);

    public static final ComponentMapper<SizeComponent> size =
            ComponentMapper.getFor(SizeComponent.class);

    public static final ComponentMapper<TextureComponent> texture =
            ComponentMapper.getFor(TextureComponent.class);

    public static final ComponentMapper<VelocityComponent> velocity =
            ComponentMapper.getFor(VelocityComponent.class);

    public static final ComponentMapper<ScoreComponent> score =
            ComponentMapper.getFor(ScoreComponent.class);

    public static final ComponentMapper<IncrementScoreComponent> incScore =
            ComponentMapper.getFor(IncrementScoreComponent.class);
}
