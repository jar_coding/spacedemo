package com.jar.space.ashley.systems.debug;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.jar.space.ashley.Mappers;
import com.jar.space.ashley.components.MeteoriteComponent;
import com.jar.space.ashley.components.PlayerComponent;
import com.jar.space.ashley.components.PositionComponent;
import com.jar.space.ashley.components.SizeComponent;
import com.jar.space.config.GameConfig;

/**
 * Created by Jar on 13.03.2017.
 */

public class DebugRenderSystem extends IteratingSystem {
    private static final Family FAMILY = Family.all(
            PositionComponent.class,
            SizeComponent.class
    ).one(PlayerComponent.class, MeteoriteComponent.class).get();

    private final ShapeRenderer renderer;

    private final Viewport viewport;

    public DebugRenderSystem(Viewport viewport) {
        super(FAMILY);
        this.viewport = viewport;
        renderer = new ShapeRenderer();
    }

    @Override
    public void update(float deltaTime) {
        Color oldColor = renderer.getColor().cpy();

        viewport.apply();
        renderer.setProjectionMatrix(viewport.getCamera().combined);

        renderer.begin(ShapeRenderer.ShapeType.Line);

        renderer.setColor(Color.RED);
        super.update(deltaTime);

        renderer.end();

        renderer.setColor(oldColor);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        PositionComponent pc = Mappers.position.get(entity);
        SizeComponent sc = Mappers.size.get(entity);

        renderer.rect(
                pc.x,
                pc.y,
                sc.width,
                sc.height);
    }

    @Override
    public boolean checkProcessing() {
        return GameConfig.DEBUG;
    }
}
