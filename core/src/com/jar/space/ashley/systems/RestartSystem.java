package com.jar.space.ashley.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.jar.space.ashley.PoolEngine;
import com.jar.space.ashley.components.CleanUpComponent;
import com.jar.space.ashley.components.GameOverComponent;
import com.jar.space.ashley.components.MeteoriteComponent;
import com.jar.space.ashley.components.PositionComponent;
import com.jar.space.ashley.components.SizeComponent;
import com.jar.space.ashley.systems.initial.ReactiveSystem;
import com.jar.space.config.GameConfig;

/**
 * Created by Marcus on 16.03.2017.
 */

public class RestartSystem extends ReactiveSystem {
    private static final Family METEOROIDS = Family.all(
            MeteoriteComponent.class,
            PositionComponent.class,
            SizeComponent.class).get();

    public final PoolEngine pool;
    public final EntityFactory factory;

    public RestartSystem(PoolEngine pool, EntityFactory factory) {
        this.pool = pool;
        this.factory = factory;
    }

    @Override
    protected Family getFamily() {
        return Family.all(GameOverComponent.class).get();
    }

    @Override
    protected void execute(Entity entity) {
        for(Entity meteoroid : pool.getEntitiesFor(METEOROIDS)) {
            meteoroid.add(pool.createComponent(CleanUpComponent.class));
        }

        factory.addPlayer(GameConfig.WORLD_WIDTH / 2, 1);
    }
}
