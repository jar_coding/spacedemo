package com.jar.space.ashley.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.jar.space.ashley.Mappers;
import com.jar.space.ashley.components.IncrementScoreComponent;
import com.jar.space.ashley.components.ScoreComponent;

/**
 * Created by Marcus on 17.03.2017.
 */

public class UpdateScoreSystem extends IteratingSystem {

    private static final Family FAMILY = Family.all(
            ScoreComponent.class,
            IncrementScoreComponent.class).get();

    public UpdateScoreSystem() {
        super(FAMILY);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        ScoreComponent scoreComponent = Mappers.score.get(entity);
        IncrementScoreComponent incrementScoreComponent = Mappers.incScore.get(entity);

        scoreComponent.score += incrementScoreComponent.value;
        entity.remove(IncrementScoreComponent.class);
    }
}
