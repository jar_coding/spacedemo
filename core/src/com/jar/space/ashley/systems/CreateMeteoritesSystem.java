package com.jar.space.ashley.systems;

import com.badlogic.ashley.systems.IntervalSystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.jar.space.config.GameConfig;

/**
 * Created by Jar on 13.03.2017.
 */

public class CreateMeteoritesSystem extends IntervalSystem {

    private static final String METEOR_BIG_1 = "meteorBig1";
    private static final String METEOR_BIG_2 = "meteorBig2";
    private static final String METEOR_MED_1 = "meteorMed1";
    private static final String METEOR_MED_2 = "meteorMed2";

    private static final float SPAWN_TIME = 0.35f;

    private final EntityFactory factory;
    private final Array<String> regionPaths;

    public CreateMeteoritesSystem(EntityFactory factory) {
        super(SPAWN_TIME);

        this.factory = factory;

        regionPaths = new Array<String>();
        regionPaths.add(METEOR_BIG_1);
        regionPaths.add(METEOR_BIG_2);
        regionPaths.add(METEOR_MED_1);
        regionPaths.add(METEOR_MED_2);
    }

    @Override
    public void updateInterval() {
        addMeteoroid();
    }

    private void addMeteoroid() {
        float size = MathUtils.random(0.5f, 1f);
        float max = GameConfig.WORLD_WIDTH - size;
        float x = MathUtils.random(0, max);
        String regionPath = regionPaths.get(MathUtils.random(0, regionPaths.size - 1));

        factory.addMeteoroid(x, GameConfig.WORLD_HEIGHT, size, regionPath);
    }
}
