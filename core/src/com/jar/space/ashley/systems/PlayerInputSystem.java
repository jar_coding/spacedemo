package com.jar.space.ashley.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.jar.space.ashley.Mappers;
import com.jar.space.ashley.components.PlayerComponent;
import com.jar.space.ashley.components.PlayerInputComponent;
import com.jar.space.ashley.components.PositionComponent;
import com.jar.space.ashley.components.VelocityComponent;

/**
 * Created by Jar on 13.03.2017.
 */

public class PlayerInputSystem extends IteratingSystem {
    private static final float SPEED = 0.15f;

    private static final Family PLAYER_FAMILY = Family.all(
            PlayerComponent.class,
            PlayerInputComponent.class,
            VelocityComponent.class,
            PositionComponent.class
    ).get();

    private float dx;
    private Vector2 touch;

    public PlayerInputSystem(final Viewport worldViewport) {
        super(PLAYER_FAMILY);

        Gdx.input.setInputProcessor(new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                switch (keycode) {
                    case Input.Keys.LEFT:
                        dx = -1;
                        break;
                    case Input.Keys.RIGHT:
                        dx = 1;
                        break;
                }
                return true;
            }

            @Override
            public boolean keyUp(int keycode) {
                resetInput();
                return true;
            }

            @Override
            public boolean touchDragged(int screenX, int screenY, int pointer) {
                setTouch(screenX, screenY);
                return true;
            }

            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                setTouch(screenX, screenY);
                return true;
            }

            @Override
            public boolean touchUp(int screenX, int screenY, int pointer, int button) {
                resetInput();
                return true;
            }

            private void setTouch(int screenX, int screenY) {
                touch = worldViewport.unproject(new Vector2(screenX, screenY));

                float x = touch.x < 0 ? touch.x - 1 : touch.x;
                float y = touch.y < 0 ? touch.y - 1 : touch.y;

                touch.set(x,y);
            }
        });
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        VelocityComponent vc = Mappers.velocity.get(entity);
        PositionComponent pc = Mappers.position.get(entity);

        if(touch != null) {
            if(playerHasNotReachedPosition(pc.x)) {
                dx = touch.x < pc.x ? -1 : 1;
            } else {
                resetInput();
            }
        }

        vc.xVelocity = dx * SPEED;
    }

    private boolean playerHasNotReachedPosition(float playerX) {
        return Math.abs(playerX - touch.x) > 0.1;
    }

    private void resetInput() {
        dx = 0;
        touch = null;
    }
}
