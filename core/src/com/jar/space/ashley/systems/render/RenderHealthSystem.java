package com.jar.space.ashley.systems.render;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.jar.space.ashley.Mappers;
import com.jar.space.ashley.components.HealthComponent;
import com.jar.space.ashley.components.PlayerComponent;
import com.jar.space.screens.HealthBarWidget;

/**
 * Created by Jar on 13.03.2017.
 */

public class RenderHealthSystem extends IteratingSystem {
    private static Family FAMILY = Family.all(
            PlayerComponent.class,
            HealthComponent.class
    ).get();

    private final HealthBarWidget healthBar;

    public RenderHealthSystem(Stage stage, TextureAtlas atlas) {
        super(FAMILY);
        this.healthBar = new HealthBarWidget(atlas);
        stage.addActor(healthBar);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        healthBar.update(Mappers.health.get(entity).health);
    }
}
