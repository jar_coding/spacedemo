package com.jar.space.ashley.systems.render;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.jar.space.ashley.Mappers;
import com.jar.space.ashley.components.PositionComponent;
import com.jar.space.ashley.components.SizeComponent;
import com.jar.space.ashley.components.TextureComponent;

/**
 * Created by Jar on 13.03.2017.
 */

public class RenderSystem extends IteratingSystem {
    private static final Family FAMILY = Family.all(
            TextureComponent.class,
            PositionComponent.class,
            SizeComponent.class
    ).get();

    private final Viewport viewport;
    private final SpriteBatch batch;

    public RenderSystem(Viewport viewport, SpriteBatch batch) {
        super(FAMILY);
        this.viewport = viewport;
        this.batch = batch;
    }

    @Override
    public void update(float deltaTime) {
        viewport.apply();
        batch.setProjectionMatrix(viewport.getCamera().combined);

        batch.begin();

        super.update(deltaTime);

        batch.end();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        TextureComponent tc = Mappers.texture.get(entity);
        PositionComponent pc = Mappers.position.get(entity);
        SizeComponent sc = Mappers.size.get(entity);

        batch.draw(tc.texture,
                pc.x,
                pc.y,
                sc.width,
                sc.height);
    }
}
