package com.jar.space.ashley.systems.render;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.jar.space.ashley.Mappers;
import com.jar.space.ashley.components.PlayerComponent;
import com.jar.space.ashley.components.ScoreComponent;
import com.jar.space.screens.ScoreBarWidget;

/**
 * Created by Jar on 13.03.2017.
 */

public class RenderScoreSystem extends IteratingSystem {
    private static Family FAMILY = Family.all(
            PlayerComponent.class,
            ScoreComponent.class
    ).get();

    private int displayScore;

    private final ScoreBarWidget pointsBar;

    public RenderScoreSystem(Stage stage, Skin skin) {
        super(FAMILY);
        this.pointsBar = new ScoreBarWidget(skin);
        stage.addActor(pointsBar);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        int score = Mappers.score.get(entity).score;

        if(displayScore < score) {
            displayScore = Math.min(score, displayScore + (int)(60 * deltaTime));
        }

        if(displayScore > score) {
            displayScore = score;
        }

        pointsBar.setScore(displayScore);
    }
}
