package com.jar.space.ashley.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.jar.space.ashley.PoolEngine;

/**
 * Created by Marcus on 16.03.2017.
 */

class EntityBuilder {
    private final PoolEngine pool;
    private Entity entity = new Entity();

    EntityBuilder(PoolEngine pool) {
        this.pool = pool;
    }

    EntityBuilder createEntity() {
        entity = pool.createEntity();
        return this;
    }

    EntityBuilder addPosition(float x, float y) {
        entity.add(pool.createPositionComponent(x,y));
        return this;
    }

    EntityBuilder addSize(float w, float h) {
        entity.add(pool.createSizeComponent(w, h));
        return this;
    }

    EntityBuilder addVelocity(float dx, float dy) {
        entity.add(pool.createVelocityComponent(dx,dy));
        return this;
    }

    EntityBuilder isPlayer() {
        entity.add(pool.createPlayerComponent());
        return this;
    }

    EntityBuilder isMeteoroid() {
        entity.add(pool.createMeteoroidComponent());
        return this;
    }

    EntityBuilder addHealth(int health) {
        entity.add(pool.createHealthComponent(health));
        return this;
    }

    EntityBuilder isControlledByPlayer() {
        entity.add(pool.createPlayerInputComponent());
        return this;
    }

    EntityBuilder addTexture(TextureRegion region) {
        entity.add(pool.createTextureComponent(region));
        return this;
    }

    EntityBuilder addPoints(int points) {
        entity.add(pool.createScoreComponent(points));
        return this;
    }

    public Entity get() {
        return entity;
    }
}
