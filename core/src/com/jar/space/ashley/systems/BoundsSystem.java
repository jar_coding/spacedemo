package com.jar.space.ashley.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.jar.space.ashley.Mappers;
import com.jar.space.ashley.components.PlayerComponent;
import com.jar.space.ashley.components.PositionComponent;
import com.jar.space.ashley.components.SizeComponent;

/**
 * Created by Jar on 13.03.2017.
 */

public class BoundsSystem extends IteratingSystem {
    private static final Family FAMILY = Family.all(
            PlayerComponent.class,
            PositionComponent.class,
            SizeComponent.class
    ).get();

    private final Viewport viewport;

    public BoundsSystem(Viewport viewport) {
        super(FAMILY);
        this.viewport = viewport;
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        PositionComponent pc = Mappers.position.get(entity);
        SizeComponent sc = Mappers.size.get(entity);

        pc.x = MathUtils.clamp(pc.x, 0, viewport.getWorldWidth() - sc.width);
        pc.y = MathUtils.clamp(pc.y, 0, viewport.getWorldHeight() - sc.height);
    }
}
