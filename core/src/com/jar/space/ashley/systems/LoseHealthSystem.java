package com.jar.space.ashley.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.jar.space.ashley.Mappers;
import com.jar.space.ashley.components.HealthComponent;
import com.jar.space.ashley.components.LoseHealthComponent;
import com.jar.space.ashley.systems.initial.ReactiveSystem;

/**
 * Created by Jar on 13.03.2017.
 */

public class LoseHealthSystem extends ReactiveSystem {
    @Override
    protected Family getFamily() {
        return Family.all(
                LoseHealthComponent.class,
                HealthComponent.class).get();
    }

    @Override
    protected void execute(Entity entity) {
        HealthComponent healthComponent = Mappers.health.get(entity);

        if(healthComponent.health > 0) {
            healthComponent.health--;
        }

        entity.remove(LoseHealthComponent.class);
    }
}
