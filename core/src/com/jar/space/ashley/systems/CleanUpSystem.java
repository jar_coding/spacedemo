package com.jar.space.ashley.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.jar.space.ashley.components.CleanUpComponent;

/**
 * Created by Marcus on 08.02.2017.
 */

public class CleanUpSystem extends IteratingSystem {
    @SuppressWarnings("unchecked")
    private static final Family FAMILY =
            Family.all(CleanUpComponent.class).get();

    public CleanUpSystem() {
        super(FAMILY);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        getEngine().removeEntity(entity);
    }
}
