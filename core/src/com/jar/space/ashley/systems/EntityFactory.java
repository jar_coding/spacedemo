package com.jar.space.ashley.systems;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.jar.space.ashley.PoolEngine;

/**
 * Created by Marcus on 16.03.2017.
 */

public class EntityFactory {
    private final EntityBuilder builder;
    private final PoolEngine pool;
    private final TextureAtlas atlas;

    public EntityFactory(PoolEngine pool, TextureAtlas atlas) {
        this.builder = new EntityBuilder(pool);
        this.pool = pool;
        this.atlas = atlas;
    }

    public void addPlayer(float x, float y) {
        float size = 0.6f;
        int health = 6;

        pool.addEntity(builder
                .createEntity()
                .isPlayer()
                .addPosition(x, y)
                .addSize(size, size)
                .addHealth(health)
                .addVelocity(0, 0)
                .isControlledByPlayer()
                .addTexture(atlas.findRegion("player"))
                .addPoints(0)
                .get());
    }

    public void addBackground(float x, float y, float w, float h) {
        pool.addEntity(builder
                .createEntity()
                .addPosition(x, y)
                .addSize(w, h)
                .addTexture(atlas.findRegion("background"))
                .get());
    }

    public void addMeteoroid(float x, float y, float size, String path) {
        pool.addEntity(builder
                .createEntity()
                .isMeteoroid()
                .addPosition(x,y)
                .addVelocity(0, -0.1f)
                .addSize(size, size)
                .addTexture(atlas.findRegion(path))
                .get());
    }
}
