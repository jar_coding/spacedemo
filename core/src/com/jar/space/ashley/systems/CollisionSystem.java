package com.jar.space.ashley.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.jar.space.ashley.Mappers;
import com.jar.space.ashley.PoolEngine;
import com.jar.space.ashley.components.CleanUpComponent;
import com.jar.space.ashley.components.MeteoriteComponent;
import com.jar.space.ashley.components.PlayerComponent;
import com.jar.space.ashley.components.PositionComponent;
import com.jar.space.ashley.components.SizeComponent;

/**
 * Created by Jar on 13.03.2017.
 */

public class CollisionSystem extends EntitySystem {
    private static final Family METEOROIDS = Family.all(
            MeteoriteComponent.class).exclude(CleanUpComponent.class).get();

    private static final Family PLAYER = Family.all(PlayerComponent.class).get();

    private final Rectangle playerHitBox;
    private final Rectangle meteoroidHitBox;
    private final PoolEngine pool;

    public CollisionSystem(PoolEngine pool) {
        this.playerHitBox = new Rectangle();
        this.meteoroidHitBox = new Rectangle();
        this.pool = pool;
    }

    @Override
    public void update(float deltaTime) {
        for(Entity player : getEngine().getEntitiesFor(PLAYER)) {
            for(Entity meteoroid : getEngine().getEntitiesFor(METEOROIDS)) {
                if(collisionDetected(player, meteoroid)) {
                    meteoroid.add(pool.createCleanUpComponent());
                    player.add(pool.createLoseHealthComponent());
                }
            }
        }
    }

    private boolean collisionDetected(Entity player, Entity meteoroid) {
        PositionComponent playerPos = Mappers.position.get(player);
        SizeComponent playerSize = Mappers.size.get(player);
        PositionComponent meteoroidPos = Mappers.position.get(meteoroid);
        SizeComponent meteoroidSize = Mappers.size.get(meteoroid);

        playerHitBox.set(playerPos.x,
                playerPos.y,
                playerSize.width,
                playerSize.height);

        meteoroidHitBox.set(meteoroidPos.x,
                meteoroidPos.y,
                meteoroidSize.width,
                meteoroidSize.height);

        return Intersector.overlaps(playerHitBox, meteoroidHitBox);
    }
}
