package com.jar.space.ashley.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.jar.space.ashley.Mappers;
import com.jar.space.ashley.PoolEngine;
import com.jar.space.ashley.components.MeteoriteComponent;
import com.jar.space.ashley.components.PlayerComponent;
import com.jar.space.ashley.components.PositionComponent;
import com.jar.space.ashley.components.SizeComponent;

/**
 * Created by Jar on 13.03.2017.
 */

public class RemoveMeteoritesSystem extends IteratingSystem {
    private static final int POINTS_PER_METEORITES = 10;

    private static final Family METEORITES = Family.all(
            MeteoriteComponent.class,
            PositionComponent.class,
            SizeComponent.class).get();

    private static final Family PLAYER = Family.all(
            PlayerComponent.class).get();

    private final PoolEngine pool;
    private Entity player;

    public RemoveMeteoritesSystem(PoolEngine pool) {
        super(METEORITES);
        this.pool = pool;
    }

    @Override
    public void update(float deltaTime) {
        player = getEngine().getEntitiesFor(PLAYER).first();
        super.update(deltaTime);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        PositionComponent pc = Mappers.position.get(entity);
        SizeComponent sc = Mappers.size.get(entity);

        if(pc.y < -sc.height) {
            entity.add(pool.createCleanUpComponent());
            player.add(pool.createIncrementScoreComponent(POINTS_PER_METEORITES));
        }
    }
}
