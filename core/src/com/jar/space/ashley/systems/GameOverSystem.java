package com.jar.space.ashley.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.jar.space.ashley.Mappers;
import com.jar.space.ashley.PoolEngine;
import com.jar.space.ashley.components.HealthComponent;
import com.jar.space.ashley.components.PlayerComponent;

/**
 * Created by Jar on 13.03.2017.
 */

public class GameOverSystem extends IteratingSystem {
    private static final Family PLAYER = Family.all(
            PlayerComponent.class,
            HealthComponent.class
    ).get();

    private final PoolEngine pool;

    public GameOverSystem(PoolEngine pool) {
        super(PLAYER);
        this.pool = pool;
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        if(Mappers.health.get(entity).health == 0) {
            entity.add(pool.createGameOverComponent());
            entity.add(pool.createCleanUpComponent());
        }
    }
}
