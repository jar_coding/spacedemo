package com.jar.space.ashley.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.jar.space.ashley.Mappers;
import com.jar.space.ashley.components.PositionComponent;
import com.jar.space.ashley.components.VelocityComponent;

/**
 * Created by Jar on 13.03.2017.
 */

public class MovementSystem extends IteratingSystem {
    private static final Family FAMILY = Family.all(
            PositionComponent.class, VelocityComponent.class
    ).get();

    public MovementSystem() {
        super(FAMILY);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        PositionComponent pc = Mappers.position.get(entity);
        VelocityComponent vc = Mappers.velocity.get(entity);

        pc.x += vc.xVelocity;
        pc.y += vc.yVelocity;
    }
}
