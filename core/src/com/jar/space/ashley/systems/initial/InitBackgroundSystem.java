package com.jar.space.ashley.systems.initial;

import com.jar.space.ashley.systems.EntityFactory;
import com.jar.space.config.GameConfig;

/**
 * Created by Jar on 13.03.2017.
 */

public class InitBackgroundSystem extends InitialSystem {
    private final EntityFactory factory;

    public InitBackgroundSystem(EntityFactory factory) {
        this.factory = factory;
    }

    @Override
    void initialise() {
        factory.addBackground(
                0,
                0,
                GameConfig.WORLD_WIDTH,
                GameConfig.WORLD_HEIGHT
        );
    }
}
