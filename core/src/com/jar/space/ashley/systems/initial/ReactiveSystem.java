package com.jar.space.ashley.systems.initial;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.ashley.core.Family;

/**
 * Created by Marcus on 15.03.2017.
 */

public abstract class ReactiveSystem extends EntitySystem implements EntityListener {

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        engine.addEntityListener(getFamily(), this);
    }

    protected abstract Family getFamily();
    protected abstract void execute(Entity entity);

    @Override
    public void entityAdded(Entity entity) {
        execute(entity);
    }

    @Override
    public void entityRemoved(Entity entity) {

    }

    @Override
    public boolean checkProcessing() {
        return false;
    }
}
