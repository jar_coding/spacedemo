package com.jar.space.ashley.systems.initial;

import com.jar.space.ashley.systems.EntityFactory;
import com.jar.space.config.GameConfig;

/**
 * Created by Jar on 13.03.2017.
 */

public class InitPlayerSystem extends InitialSystem{

    private static final float PLAYER_X = GameConfig.WORLD_WIDTH / 2;
    private static final float PLAYER_Y = 1;

    private final EntityFactory factory;

    public InitPlayerSystem(EntityFactory factory) {
        this.factory = factory;
    }

    @Override
    void initialise() {
        factory.addPlayer(PLAYER_X, PLAYER_Y);
    }
}
