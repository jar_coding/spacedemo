package com.jar.space.ashley.systems.initial;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.EntitySystem;

/**
 * Created by Jar on 13.03.2017.
 */

public abstract class InitialSystem extends EntitySystem {

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        initialise();
    }

    abstract void initialise();

    @Override
    public boolean checkProcessing() {
        return false;
    }
}
