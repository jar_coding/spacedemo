package com.jar.space.ashley.components;

import com.badlogic.ashley.core.Component;

/**
 * Created by Jar on 13.03.2017.
 */

public class VelocityComponent implements Component {
    public float xVelocity;
    public float yVelocity;
}
