package com.jar.space.ashley.components;

import com.badlogic.ashley.core.Component;

/**
 * Created by Marcus on 17.03.2017.
 */

public class ScoreComponent implements Component {
    public int score;
}
