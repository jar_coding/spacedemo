package com.jar.space.ashley.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Jar on 13.03.2017.
 */

public class TextureComponent implements Component {
    public TextureRegion texture;
}
