package com.jar.space.ashley;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.jar.space.ashley.components.IncrementScoreComponent;
import com.jar.space.ashley.components.CleanUpComponent;
import com.jar.space.ashley.components.GameOverComponent;
import com.jar.space.ashley.components.HealthComponent;
import com.jar.space.ashley.components.LoseHealthComponent;
import com.jar.space.ashley.components.MeteoriteComponent;
import com.jar.space.ashley.components.PlayerComponent;
import com.jar.space.ashley.components.PlayerInputComponent;
import com.jar.space.ashley.components.ScoreComponent;
import com.jar.space.ashley.components.PositionComponent;
import com.jar.space.ashley.components.SizeComponent;
import com.jar.space.ashley.components.TextureComponent;
import com.jar.space.ashley.components.VelocityComponent;

/**
 * Created by Marcus on 15.03.2017.
 */

public class PoolEngine extends PooledEngine {

    public PositionComponent createPositionComponent(float x, float y) {
        PositionComponent component = createComponent(PositionComponent.class);
        component.x = x;
        component.y = y;
        return component;
    }

    public CleanUpComponent createCleanUpComponent() {
        CleanUpComponent component = createComponent(CleanUpComponent.class);
        return component;
    }

    public GameOverComponent createGameOverComponent() {
        GameOverComponent component = createComponent(GameOverComponent.class);
        return component;
    }

    public HealthComponent createHealthComponent(int health) {
        HealthComponent component = createComponent(HealthComponent.class);
        component.health = health;
        return component;
    }

    public LoseHealthComponent createLoseHealthComponent() {
        LoseHealthComponent component = createComponent(LoseHealthComponent.class);
        return component;
    }

    public MeteoriteComponent createMeteoroidComponent() {
        MeteoriteComponent component = createComponent(MeteoriteComponent.class);
        return component;
    }

    public PlayerComponent createPlayerComponent() {
        PlayerComponent component = createComponent(PlayerComponent.class);
        return component;
    }

    public SizeComponent createSizeComponent(float width, float height) {
        SizeComponent component = createComponent(SizeComponent.class);
        component.width = width;
        component.height = height;
        return component;
    }

    public TextureComponent createTextureComponent(TextureRegion region) {
        TextureComponent component = createComponent(TextureComponent.class);
        component.texture = region;
        return component;
    }

    public VelocityComponent createVelocityComponent(float dx, float dy) {
        VelocityComponent component = createComponent(VelocityComponent.class);
        component.xVelocity = dx;
        component.yVelocity = dy;
        return component;
    }

    public PlayerInputComponent createPlayerInputComponent() {
        PlayerInputComponent component = createComponent(PlayerInputComponent.class);
        return component;
    }

    public Component createScoreComponent(int score) {
        ScoreComponent component = createComponent(ScoreComponent.class);
        component.score = score;
        return component;
    }

    public Component createIncrementScoreComponent(int value) {
        IncrementScoreComponent component = createComponent(IncrementScoreComponent.class);
        component.value = value;
        return component;
    }
}
