package com.jar.space.config;

/**
 * Created by Marcus on 29.01.2017.
 */

public final class GameConfig {
    public static final boolean DEBUG = false;

    public static final float WIDTH = 480; // pixels
    public static final float HEIGHT = 800; // pixels

    public static final float HUD_WIDTH = WIDTH; // world units
    public static final float HUD_HEIGHT = HEIGHT; // world units

    public static final float WORLD_WIDTH = 6f; // world units
    public static final float WORLD_HEIGHT = 10f; // world units

    private GameConfig() {}
}
